#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include <omp.h> 
#include "dist.c"

int main(int argc, char *argv[]){
	if (argc < 3){
		printf("Useage a.out n numthreads\n");
		return 0;
	}
//	printf("test = %lf\n", euclideanDist(0.5,0.5,0.7,0.2));
	int n;	
	int numthreads = atoi(argv[2]);
    
    
    omp_set_num_threads(numthreads);
    	
    if (atoi(argv[1]) == 0)
    	n = INT_MAX;
    else
    	n = atoi(argv[1]);
    int hits = 0;
    int i;
    //srand(time(0));
    double start = omp_get_wtime();	 // Get start time
    double x,y,cx = 0.5, cy=0.5;
    int seed;
		
			

    /*--------------------Starting multi-threading--------------------*/
        
    /* 1. We need x, y, and seed to be private because they will all be getting written into by every thread
       2. We use reduction to bring the sum of hits in all threads together to the main thread */          
    #pragma omp parallel for private(x,y,seed) reduction (+:hits)	
    for (i=0;i<n;i++){
            // We use the thread id to generate the first seed used for our random coordinates
            // Which is only when i == 0
    		if(i==0){ int seed = omp_get_thread_num();}
    		else seed *= i; // if i is not 0 then seed is updated by multiplying it by current i
    		
            // Obtaining random coordinates by using thread safe rand (rand_r) and our current seed
            x = ((double)(rand_r(&seed)%1000))/1000;
			seed *= i*7;
    		y = ((double)(rand_r(&seed)%1000))/1000;
    		
        //	printf("(%lf,%lf)\n", x,y);
    	//	printf("dist = %lf\n", euclideanDist(cx,cy,x,y));
    			
            // Checking if dart's coordinates are within the unit circle and
            // adding one to the number of hits if that is the case
            if (euclideanDist(cx,cy,x,y) < 0.5){
    			hits++;
    		}
    }

        
    double end = omp_get_wtime(); // Get end time
    	
       
	printf("hits = %d\n", hits);

    double pi = 4*((double)hits/(double)n); // Calculate pi
    	
    printf("pi = %.20lf\n",pi );
    printf("Time for %d loops on %d threads = %.16g\n",n,numthreads,end-start);
	   

    





}
