#!/usr/bin/env
import csv
import matplotlib.pyplot as plt
from operator import itemgetter
sends = []
receives = []
with open('output.txt') as csv_file:
	reader = csv.DictReader(csv_file)
	for row in reader:
		if row ['Type'] == 'S':
			sends.append(row)
		else: receives.append(row)

sort_on = 'Bytes'
ns = [(int(s[sort_on]),s) for s in sends]
ns.sort()
sortedsends = [s for (k,s) in ns]
ns = [(int(s[sort_on]),s) for s in receives]
ns.sort()
sortedreceives = [s for (k,s) in ns]

sxvals = []
syvals = []
for i in sortedsends:
	sxvals.append(int(i['Bytes']))
	syvals.append(int(i['Time']))

rxvals = []
ryvals = []
for i in sortedreceives:
	rxvals.append(int(i['Bytes']))
	ryvals.append(int(i['Time']))

plt.plot(sxvals,syvals)
plt.title("Sending Size vs Time")
plt.ylabel("Time(ms)")
plt.xlabel("Bytes(multiples of 2)")
plt.show()
plt.plot(rxvals,ryvals)
plt.ylabel("Time(ms)")
plt.xlabel("Bytes(multiples of 2)")
plt.title("Receiving Size vs Time")
plt.show()

with open("graph_data.txt","w") as outfile:
	outfile.write("Sending Graph\n(Bytes,Time)\n(x,y)\n")
	for i in range(0,len(sxvals)):
		outfile.write("("+str(sxvals[i])+","+str(syvals[i])+")\n")

	outfile.write("Receiving Graph\n(Bytes,Time)\n(x,y)\n")
	for i in range(0,len(rxvals)):
		outfile.write("("+str(rxvals[i])+","+str(ryvals[i])+")\n")


