#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <sys/time.h>
#include <assert.h>

//Implementation taken from:
//https://stackoverflow.com/questions/101439/the-most-efficient-way-to-implement-an-integer-based-power-function-powint-int
int ipow(int base, int exp)
{
    int result = 1;
    for (;;)
    {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        if (!exp)
            break;
        base *= base;
    }

    return result;
}

int main(int argc, char *argv[]){
    int rank, p, i;
    struct timeval t1, t2;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &p);

    assert(p>=2);

    for(i = 0; i < 20; i++ ){
        int num_bytes = ipow(2,i);
            if(rank == 4){
                int dest = 0;
                
                char *sendBuffer = (char*)malloc(sizeof(char)*num_bytes);
                int index = 0;
                while(index < num_bytes){
                    sendBuffer[index] = 'x';
                    index++;
                }
                gettimeofday(&t1, NULL);
                MPI_Send(sendBuffer, num_bytes ,MPI_CHAR, dest, 0, MPI_COMM_WORLD);
                gettimeofday(&t2, NULL);
                int tSend = (t2.tv_sec - t1.tv_sec) + (t2.tv_usec - t1.tv_usec)*1000 / 1000;
                
                printf("Rank = %d: sent message of %d bytes to rank %d; Send time %d millisec\n", rank, num_bytes, dest, tSend);
            } else if (rank == 0){
                MPI_Status status;
                char *recvBuffer = (char*)malloc(sizeof(char)*num_bytes);
                gettimeofday(&t1, NULL);
                MPI_Recv(recvBuffer, num_bytes, MPI_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
                gettimeofday(&t2, NULL);
                int tRecv = (t2.tv_sec - t1.tv_sec) + (t2.tv_usec - t1.tv_usec)*1000 / 1000;
                printf("Rank = %d: received message of %d bytes from rank %d; Recv time %d millisec\n", rank, num_bytes, status.MPI_SOURCE, tRecv);
                //printf("%s", recvBuffer);
            }            
    }
        MPI_Finalize();
}
