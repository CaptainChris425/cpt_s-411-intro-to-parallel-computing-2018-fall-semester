#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include <math.h>
#include <mpi.h>
#include <assert.h>
#include "matrixoperations.h"


 int main( int argc, char *argv[]){
	//User must input A B and a large Prime P
	// A and B for the initial M matrix [A 0] [B 1]
	if (argc < 5){
		printf("Useage a.out a b P X_0\n");
		return 0;
	}
	int A = atoi(argv[1]);
	int B = atoi(argv[2]);
	int P = atoi(argv[3]);
	int X_0 = atoi(argv[4]);
	int rank,p;
	int n = 32;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &p);
	assert(p>=2);
	assert((n/p)%2 == 0);
	//Creating a 3d matirx to hold the many M(2x2) matrixies
	//n x 2 x 2
	long int ***Mmatrix = (long int ***)malloc(sizeof(long int**)*n/p);
	//use a temp to store for next timestep
	long int ***temp = (long int ***)malloc(sizeof(long int**)*n/p);
	int i;
	for (i=0; i < (n/p); i++){
		Mmatrix[i] = (long int **)malloc(sizeof(long int *)*2);
		temp[i] = (long int **)malloc(sizeof(long int *)*2);
	}
	for (i=0; i < (n/p); i++){
		Mmatrix[i][0] = (long int*)malloc(sizeof(long int)*2);
		Mmatrix[i][1] = (long int*)malloc(sizeof(long int)*2);
		temp[i][0] = (long int*)malloc(sizeof(long int)*2);
		temp[i][1] = (long int*)malloc(sizeof(long int)*2);
	}
	//Done with matrix allocation. Empty matricies right now	
	//if you have a rank you can send it to in range
			//Send global to its rank + 2^timestep//if you have a rank you can send it to in range
			//Send global to its rank + 2^timestep//Start matrix 1 as identity matrix
	// all other matricies start as M
	// where M =
	//  [A 0]
	//  [B 1]
	Mmatrix[0][0][0] = 1; 
	Mmatrix[0][0][1] = 0;
	Mmatrix[0][1][0] = 0;
	Mmatrix[0][1][1] = 1;
	if(rank == 0){
		i = 1;
	} else {
		i = 0;
	}

	for (; i < (n/p); i++){
		Mmatrix[i][0][0] = A;
		Mmatrix[i][0][1] = 0;
		Mmatrix[i][1][0] = B;
		Mmatrix[i][1][1] = 1;
	}
	
	//Just to print out a random rank's array of M's
	// just for debugging
	/* if (rank == 4){
		for(i = 0; i< n/p; i++){
			printf("%d \n", i);
			printf("| %li | %li |\n", Mmatrix[i][0][0], Mmatrix[i][0][1]);
			printf("| %li | %li |\n", Mmatrix[i][1][0], Mmatrix[i][1][1]);
		}
	} */


	//Start of N-value parallel prefix
	for(i=0; i < (log((n/p))/log(2)); i++){
		//for each timestep 0-log2(n) where n = n/p
		int j;
		//start of parallel region
		#pragma omp parallel for
		for(j = 0; j < n/p;j++){
			//for every value in the array
			int pw = pow(2,i);               
			

			//pw = 2 ^ the timestep
			if (j<pw){
				//if the index is lower than 2^ts then we just copy its local to its global
				temp[j] = Mmatrix[j];
			}else{
				//else the index is bigger than 2^ts
				// then we multiply its local to index - pw and make that its global
				temp[j] = twoby2times2by2(Mmatrix[j],Mmatrix[j-pw]);
			}
		}
		//Copy all globals into locals
		memcpy(Mmatrix,temp,sizeof(long int**)*(n/p));	
	}


	//At this polong int every rank holds an array 
	// A = [M^0, M^1, M^2, ... , M^n/p-1]

	//prlong inting a random array's array of M's
	/* if (rank == 1){
		for(i = 0; i< n/p; i++){
			printf("%d \n", i);
			printf("| %li | %li |\n", Mmatrix[i][0][0], Mmatrix[i][0][1]);
			printf("| %li | %li |\n", Mmatrix[i][1][0], Mmatrix[i][1][1]);
		}
	} */
	
	//Start P-value parallel prefix
	
	long int **local = (long int**)malloc(sizeof(long int*)*2);
	long int **global = (long int**)malloc(sizeof(long int*)*2);
	int k;

	for (k = 0; k < 2; k++){
		global[k] = (long int*)malloc(sizeof(long int)*2);
		local[k] = (long int*)malloc(sizeof(long int)*2);
	}
		
	long int **recvglobal = (long int**)malloc(sizeof(long int*)*2);
	for (k = 0; k < 2; k++){
		recvglobal[k] = (long int*)malloc(sizeof(long int)*2);
	}

	global[0][0] = local[0][0] = Mmatrix[n/p-1][0][0];
	global[1][0] = local[1][0] = Mmatrix[n/p-1][1][0];
	global[0][1] = local[0][1] = Mmatrix[n/p-1][0][1];
	global[1][1] = local[1][1] = Mmatrix[n/p-1][1][1];
    
	MPI_Status status[2];
	MPI_Request request[2];
	int mate,v;
	for(i = 0; i < log(p)/log(2); i++){
		MPI_Barrier(MPI_COMM_WORLD);
		v = pow(2,i);
		mate = rank ^ v;
		v = v << 1;
		if(rank == 1){
			printf("Timestep: %d\nRank: %d global[0] %li | %li\n", i, rank, global[0][0], global[0][1]);
			printf("Rank: %d global[1] %li | %li\n", rank, global[1][0], global[1][1]);
		}
		//printf("Rank %d sending to %d at timestep %d\n", rank, mate, i);
		MPI_Isend(global[0],2, MPI_LONG, mate, 0, MPI_COMM_WORLD, &request[0]);
		MPI_Recv(recvglobal[0], 2, MPI_LONG, mate, MPI_ANY_TAG, MPI_COMM_WORLD, &status[0]);
		MPI_Wait(&request[0], &status[0]);
		MPI_Isend(global[1],2, MPI_LONG, mate, 0, MPI_COMM_WORLD, &request[1]);
		MPI_Recv(recvglobal[1], 2, MPI_LONG, mate, MPI_ANY_TAG, MPI_COMM_WORLD, &status[1]);
		MPI_Wait(&request[1], &status[1]);

		if(rank == 7){
			
			printf("Timestep: %d\nRank: %d recvglobal[0] %li | %li\n", i, rank, recvglobal[0][0], recvglobal[0][1]);
			printf("Rank: %d recvglobal[1] %li | %li\n", rank, recvglobal[1][0], recvglobal[1][1]);
		}
		global = twoby2times2by2(global,recvglobal);

		if(mate < rank){
			local = twoby2times2by2(local,recvglobal);
		}

	}

	/* for(i=0; i<log(p)/log(2); i++){
		//For each timestep 0 - log2(n) where n = p
		//copy global into local
		long int **recvglobal = (long int**)malloc(sizeof(long int*)*2);
		

		for (k = 0; k < 2; k++){
			recvglobal[k] = (long int*)malloc(sizeof(long int)*2);
		}

		local[0][0] = global[0][0];
		local[1][0] = global[1][0];
		local[0][1] = global[0][1];
		local[1][1] = global[1][1];
		
		MPI_Barrier(MPI_COMM_WORLD);
		if(rank + pow(2,i) < p){
			//if you have a rank you can send it to in range
			//Send global to its rank + 2^timestep
			
			MPI_Send(global[0],2, MPI_LONG,rank + pow(2,i), 0, MPI_COMM_WORLD);
			MPI_Send(global[1],2, MPI_LONG, rank + pow(2,i), 0, MPI_COMM_WORLD);
		}	
		int j;
		for(j=0; j<p; j++){
			//for every rank (this is some funky logic but it works well
			if(j+pow(2,i) == rank){
				//if there was a rank that had sent to the current rank
				// we can check this by saying for every possible rank value if
								//its rank + 2^ts is the rank we are on
								// then we know it needs to recieve a value
				//recieve global value
				MPI_Recv(recvglobal[0], 2, MPI_LONG, rank-pow(2,i),MPI_ANY_TAG,MPI_COMM_WORLD, &status[0]);
				MPI_Recv(recvglobal[1], 2, MPI_LONG, rank-pow(2,i),MPI_ANY_TAG,MPI_COMM_WORLD, &status[1]);

				
				
				//if it had recieved from a lower rank then multiply the global by the recieved global
				global = twoby2times2by2(global,recvglobal);
				
			}
				
		}
		
		

	} */

	/* if(rank == 0 || rank == 1){
		printf("Rank: %d\n global[0] %li %li\n", rank, global[0][0], global[0][1]);
		printf("Rank: %d\n global[1] %li %li\n", rank, global[1][0], global[1][1]);
	} */
	
	//
	long int **offset = (long int**)malloc(sizeof(long int*)*2);
	
	for (k = 0; k < 2; k++){
		offset[k] = (long int*)malloc(sizeof(long int)*2);
	}
	int j;
	
	/* if(rank == 1){
		printf("Rank: %d global[0] %li | %li\n", rank, global[0][0], global[0][1]);
		printf("Rank: %d global[1] %li | %li\n", rank, global[1][0], global[1][1]);
	} */
	
	if(rank != p - 1){ // if process is not the last one
		MPI_Isend(global[0], 2, MPI_LONG, rank + 1, 0, MPI_COMM_WORLD, &request[0]);
		
	} 
	if(rank != 0){ // if process is not the first one
		MPI_Recv(offset[0], 2, MPI_LONG, rank - 1, MPI_ANY_TAG, MPI_COMM_WORLD, &status[0]);
	} 
	
	if(rank != p - 1){ // if process is not the last one
		MPI_Isend(global[1], 2, MPI_LONG, rank + 1, 0, MPI_COMM_WORLD, &request[0]);
	} 
	if(rank != 0){ // if process is not the first one
		MPI_Recv(offset[1], 2, MPI_LONG, rank - 1, MPI_ANY_TAG, MPI_COMM_WORLD, &status[0]);
	} 

	/* if(rank == 2){
		printf("Rank: %d offset[0] %li | %li\n", rank, offset[0][0], offset[0][1]);
		printf("Rank: %d offset[1] %li | %li\n", rank, offset[1][0], offset[1][1]);
	} */

	// Updating each matrix with the offset
	if(rank != 0){
		#pragma omp parallel for
		for(i=0;i<n/p;i++){
			Mmatrix[i] = twoby2times2by2(Mmatrix[i], offset);
		}
	}
	
	long int* X_i = (long int*) malloc(sizeof(long int)*n/p);
	long int* base = (long int*)malloc(sizeof(long int)*2);
	base[0] = X_0, base[1] = 1;
	
	//#pragma omp parallel for
	for(i=0;i<n/p;i++){
		/* if(rank == 2){
			printf("Rank: %d Mmatrix[0] %li | %li\n", rank, Mmatrix[i][0][0], Mmatrix[i][0][1]);
			printf("Rank: %d Mmatrix[1] %li | %li\n", rank, Mmatrix[i][1][0], Mmatrix[i][1][1]);

		} */
		X_i[i] = oneby2times2by2(base, Mmatrix[i], P);
		if(rank == 0 || rank == 1 || rank == 2){
			printf("Rank: %d X_i[%d] = %li\n", rank, i, X_i[i]);
		}
	}
	
//AFTER THIS
//we need to send every global value to the next rank up
	//then add it to every value in the array
			//serially - not by parallel prefix
				//just straight add the value to everyone in the array

//once we do that
//then we have all the arrays in all the ranks to have their correct M matricies
//after that then we just multiply an array [x0 1] with all the M's to come up with the random numbers
//then we have to gather the arrays into a single rank so we can output them in order
			//or just have to print them in order
 




/*
	int recievedglobal;
	if(rank+1 < p)
		MPI_Send(&global, 1 , MPI_INT, rank+1, 0, MPI_COMM_WORLD);
	if(rank-1 > 0)
		MPI_Recv(&recievedglobal, 1, MPI_INT, rank-1, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
//	printf("My rank is %d and my recvglobal is %d",rank,recievedglobal);
	MPI_Barrier(MPI_COMM_WORLD);
	global = recievedglobal;
	for(i=0;i<n/p;i++){
		A[i] = A[i] + global;
	}
	for(i=0;i<p;i++){
		MPI_Barrier(MPI_COMM_WORLD);
		if(i==rank){
			for (i=0; i< (n/p); i++){
				printf("A[%d] = %d\n", i, A[i]);
			}
		}
	}


//
	if(rank == 1){
		printf("My rank is %d\n", rank);
		for (i=0; i< (n/p); i++){
			printf("A[%d] = %d\n", i, A[i]);
		}
		printf("The global value we got was %d",local);
	}
	if(rank == 0){
		printf("My rank is %d\n", rank);
		for (i=0; i< (n/p); i++){
			printf("A[%d] = %d\n", i, A[i]);
		}
		printf("The global value we got was %d",local);
	}

*/






	MPI_Finalize();
}
