import matplotlib.pyplot as plt
n = [1,2,4,8,16,32]
_64 = [1000,582,310,200,156,102]
_128 = [2078,1010,689,412,255,175]
_256 = [5100,2600,1420,780,420,250]
_512 = [11029,5900,2900,1580,780,390]

su64 = []
su128= []
su256= []
su512= []

for i in _64:
	su64.append(_64[0]/i)
for i in _128:
	su128.append(_128[0]/i)
for i in _256:
	su256.append(_256[0]/i)
for i in _512:
	su512.append(_512[0]/i)

e64 = []
e128= []
e256= []
e512= []
p = [1,2,5,8,16,32]
for i in range(0,len(n)):
	e64.append(su64[i]/p[i])
	e128.append(su128[i]/p[i])
	e256.append(su256[i]/p[i])
	e512.append(su512[i]/p[i])

plt.plot(n,_64)
plt.plot(n,_128)
plt.plot(n,_256)
plt.plot(n,_512)
plt.title('Parallel Runtime');
plt.legend(['n=64','n=128','n=256','n=512'])
plt.show()
plt.plot(n,su64)
plt.plot(n,su128)
plt.plot(n,su256)
plt.plot(n,su512)
plt.title('Speedup')
plt.legend(['n=64','n=128','n=256','n=512'])
plt.show()
plt.plot(n,e64)
plt.plot(n,e128)
plt.plot(n,e256)
plt.plot(n,e512)
plt.title('Efficency')
plt.legend(['n=64','n=128','n=256','n=512'])
plt.show()
